package com.example.locationfinder;

import android.util.Log;

import com.google.android.gms.location.Geofence;

/**
 * A single Geofence object, defined by its center, radius and address
 */
public class SimpleGeofence 
{
	private static final String TAG = "SimpleGeofence";
	private final String mId;
	private final double mLatitude;
	private final double mLongitude;
	private final float mRadius;
	private long mExpirationDuration;
	private int mTransitionType;
	private final String mAddress;

	/**
	 * @param geofenceId
	 *            The Geofence's request ID
	 * @param latitude
	 *            Latitude of the Geofence's center.
	 * @param longitude
	 *            Longitude of the Geofence's center.
	 * @param radius
	 *            Radius of the geofence circle.
	 * @param expiration
	 *            Geofence expiration duration
	 * @param transition
	 *            Type of Geofence transition.
	 */
	public SimpleGeofence(String geofenceId, double latitude, double longitude,
			float radius, long expiration, int transition, String address) 
	{
		this.mId = geofenceId;
		this.mLatitude = latitude;
		this.mLongitude = longitude;
		this.mRadius = radius;
		this.mExpirationDuration = expiration;
		this.mTransitionType = transition;
		this.mAddress = address;
		Log.d(TAG, geofenceId + " " + latitude + " " + longitude + " " + address);
	}

	// Instance field getters
	public String getId() 
	{
		return mId;
	}

	public double getLatitude() 
	{
		return mLatitude;
	}

	public double getLongitude() 
	{
		return mLongitude;
	}

	public float getRadius() 
	{
		return mRadius;
	}

	public long getExpirationDuration() 
	{
		return mExpirationDuration;
	}

	public int getTransitionType() 
	{
		return mTransitionType;
	}

	public String getAddress() 
	{
		return mAddress;
	}

	/**
	 * Creates a Location Services Geofence object from a SimpleGeofence.
	 * 
	 * @return A Geofence object
	 */
	public Geofence toGeofence() 
	{
		// Build a new Geofence object
		return new Geofence.Builder().setRequestId(getId())
				.setTransitionTypes(mTransitionType)
				.setCircularRegion(getLatitude(), getLongitude(), getRadius())
				.setExpirationDuration(mExpirationDuration).build();
	}
}
