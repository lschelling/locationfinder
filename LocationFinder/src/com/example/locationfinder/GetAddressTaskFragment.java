package com.example.locationfinder;

import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

/**
 * This Fragment manages a single GetAddress task and retains 
 * itself across configuration changes.
 */
public class GetAddressTaskFragment extends Fragment 
{
  static final String TAG = "TaskFragment";
   
  /**
   * Callback interface through which the fragment will report the
   * task's progress and results back to the Activity.
   */
  public static interface TaskCallbacks {
    void onPreExecute();
    void onProgressUpdate(int percent);
    void onCancelled();
    void onPostExecute();
  }

  private String mAddress;
  private Context mContext;
  private TaskCallbacks mCallbacks;
  private GetAddressAsync mTask;

  public String getAddress()
  {
	  return mAddress;
  }
  
  public void findAddress(double latitude, double longitude)
  {
	  // Create and execute the background task.
	  mTask = new GetAddressAsync();
	  mTask.execute(latitude, longitude);
  }
 
  /**
   * Hold a reference to the parent Activity so we can report the
   * task's current progress and results. The Android framework 
   * will pass us a reference to the newly created Activity after 
   * each configuration change.
   */
  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    mContext = activity.getBaseContext();
    mCallbacks = (TaskCallbacks) activity;
  }

  /**
   * This method will only be called once when the retained
   * Fragment is first created.
   */
  @Override
  public void onCreate(Bundle savedInstanceState) 
  {
    super.onCreate(savedInstanceState);
    // Retain this fragment across configuration changes.
    setRetainInstance(true);
  }

  /**
   * Set the callback to null so we don't accidentally leak the 
   * Activity instance.
   */
  @Override
  public void onDetach() {
    super.onDetach();
    mCallbacks = null;
  }

  /**
   * Async Task to get the address from a location
   */
  public class GetAddressAsync extends AsyncTask<Double, Integer, Address> 
  {
	
	public GetAddressAsync() 
	{
		super();
	}

	/**
	 * This is where the work is done. Get the address using the Geocoder
	 */
	@Override
	protected Address doInBackground(Double... params) 
	{
		Geocoder geocoder = new Geocoder(mContext);
		double latitude = params[0].doubleValue();
		double longitude = params[1].doubleValue();
		List<Address> addresses = null;

		try
		{
			addresses = geocoder.getFromLocation(latitude, longitude,1);
		} 
		catch (IOException e) 
		{
			Log.d(TAG, "destroy");
			e.printStackTrace();
		}

		if(addresses != null && addresses.size() > 0 )
		{
			Address address = addresses.get(0);
			return address;
		}
		return null;
	}

	@Override
    protected void onProgressUpdate(Integer... percent) {
      if (mCallbacks != null) {
        mCallbacks.onProgressUpdate(percent[0]);
      }
    }

    @Override
    protected void onCancelled() {
      if (mCallbacks != null) {
        mCallbacks.onCancelled();
      }
    }
    
    private String printPrettyLocation(Address address)
    {
		String addressStr = new String();
		if(null != address)
		{	
			for (int i = 0; i < address.getMaxAddressLineIndex(); i++) 
			{
				addressStr += address.getAddressLine(i) + System.getProperty("line.separator");
			}
			addressStr += address.getCountryName();
			Log.d(TAG, "Broadcast:" + addressStr);
			
		}
		else
		{
			Log.d(TAG, "Broadcast:" + getString(R.string.msg_no_location_found));
			addressStr = getString(R.string.msg_no_location_found);	
		}
		return addressStr;
    }
    @Override
    protected void onPostExecute(Address address) 
    {
      mAddress = printPrettyLocation(address);
      if (mCallbacks != null)
      {
        mCallbacks.onPostExecute();
      }
    }
  }
}
