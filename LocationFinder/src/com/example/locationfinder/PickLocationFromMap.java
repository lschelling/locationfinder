package com.example.locationfinder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class PickLocationFromMap extends FragmentActivity
{
	public static GoogleMap map;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pick_location_from_map);

		// create map
        map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        map.setMyLocationEnabled(true);
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //  get extra strings passed to the activity
        String lat = getIntent().getStringExtra("latitude");
        String lon = getIntent().getStringExtra("longitude");     
        if ((lat.length() > 0) && (lon.length() > 0))
        {
        	// set a marker at the currently selected position
        	LatLng pos = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
        	map.addMarker(new MarkerOptions()
	        	.position(pos)
	        	.draggable(false)
	        	.title(getString(R.string.marker_currently_selected)));
        	// move the camera to the currently selected position and zoom in
        	CameraPosition cameraPosition = CameraPosition.fromLatLngZoom(pos, (float) 14.0);
        	map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }

        // add OnMapClickListener for the selection of the new position
		map.setOnMapClickListener(new OnMapClickListener() 
		{
			@Override
			public void onMapClick(LatLng point) 
			{
				LatLng pos = new LatLng(point.latitude, point.longitude);
	        	map.addMarker(new MarkerOptions()
		        	.position(pos)
		        	.draggable(false));
	        	
				Toast.makeText(getBaseContext(),
						getString(R.string.toast_newly_selected) + point.latitude + " " + point.longitude, Toast.LENGTH_LONG).show();
				transit(point);
			}
		});
	}

	// put extras with the new location for the MainActivity
	private void transit(LatLng point) {
		Intent intent = new Intent(this, MainActivity.class);
		intent.putExtra("latitude", Double.toString(point.latitude));
		intent.putExtra("longitude", Double.toString(point.longitude));
		this.startActivity(intent);
		this.finish();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        this.finish();
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}

}
