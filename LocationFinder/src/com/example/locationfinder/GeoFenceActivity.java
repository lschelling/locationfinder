package com.example.locationfinder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.example.locationfinder.LocationFinderUtils.REMOVE_TYPE;
import com.example.locationfinder.LocationFinderUtils.REQUEST_TYPE;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.Geofence;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

/**
 * code from 
 * @see http://developer.android.com/training/location/geofencing.html
 */
public class GeoFenceActivity extends FragmentActivity
{
	static final String TAG = "GEOFENCE_ACTIVITY";
	private static final int accuracy = 50;
    private static final long GEOFENCE_EXPIRATION_IN_HOURS = 12;
	private static final long GEOFENCE_EXPIRATION_IN_MILLISECONDS =  GEOFENCE_EXPIRATION_IN_HOURS * DateUtils.HOUR_IN_MILLIS;
	    
    // Store the current request
    private REQUEST_TYPE mRequestType;

    // Store the current type of removal
    private REMOVE_TYPE mRemoveType;
   
    // Persistent storage for geofences
    private SimpleGeofenceStore mPrefs;
    
    // Store a list of geofences to add
    List<Geofence> mCurrentGeofences;
        
     // Internal lightweight geofence object
    private SimpleGeofence mUIGeofence;
    
    // Add geofences handler
    private GeofenceRequester mGeofenceRequester;
    
    // Remove geofences handler
    private GeofenceRemover mGeofenceRemover;
    
    // An instance of an inner class that receives broadcasts from listeners and from the
    // IntentService that receives geofence transition events
    private GeofenceSampleReceiver mBroadcastReceiver;

    // An intent filter for the broadcast receiver
    private IntentFilter mIntentFilter;

    // Store the list of geofences to remove
    private List<String> mGeofenceIdsToRemove;

    // text view for the location and address
    TextView mLatitude;
    TextView mLongitude;
    TextView mAddress;
    
    @Override
	protected void onCreate(Bundle savedInstanceState) 
    {
		super.onCreate(savedInstanceState);
		servicesConnected() ;
		setContentView(R.layout.activity_geofence);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		mLatitude = (TextView)findViewById(R.id.tvLatitude);
		mLatitude.setText(getIntent().getStringExtra("latitude"));
		mLongitude = (TextView)findViewById(R.id.tvLongitude);
		mLongitude.setText(getIntent().getStringExtra("longitude"));
		mAddress = (TextView)findViewById(R.id.tvAddress);
		mAddress.setText(getIntent().getStringExtra("address"));

        // Create a new broadcast receiver to receive updates from the listeners and service
        mBroadcastReceiver = new GeofenceSampleReceiver();

        // Create an intent filter for the broadcast receiver
        mIntentFilter = new IntentFilter();

        // Action for broadcast Intents that report successful addition of geofences
        mIntentFilter.addAction(LocationFinderUtils.ACTION_GEOFENCES_ADDED);

        // Action for broadcast Intents that report successful removal of geofences
        mIntentFilter.addAction(LocationFinderUtils.ACTION_GEOFENCES_REMOVED);

        // Action for broadcast Intents containing various types of geofencing errors
        mIntentFilter.addAction(LocationFinderUtils.ACTION_GEOFENCE_ERROR);

        // All Location Services sample apps use this category
        mIntentFilter.addCategory(LocationFinderUtils.CATEGORY_LOCATION_SERVICES);

        // Instantiate a new geofence storage area
        mPrefs = new SimpleGeofenceStore(this);

        // Instantiate the current List of geofences
        mCurrentGeofences = new ArrayList<Geofence>();

        mGeofenceIdsToRemove = new ArrayList<String>();
        // Instantiate a Geofence requester
        mGeofenceRequester = new GeofenceRequester(this);

        // Instantiate a Geofence remover
        mGeofenceRemover = new GeofenceRemover(this);     

        if( (0 < mLatitude.getText().toString().length()) &&
				(0 < mLongitude.getText().toString().length()))
        {
        	mUIGeofence = mPrefs.getGeofence("1");
            if (mUIGeofence != null) 
            {
            	onUnregisterGeofence();	
            }
       	   	onRegisterGeofence();
        	mUIGeofence = mPrefs.getGeofence("1");
        	Log.d(TAG, "Set new location to geofence");
        }
        else
        {
        	mUIGeofence = mPrefs.getGeofence("1");
        	Log.d(TAG, "Keep old Location");
            if (mUIGeofence != null) 
            {
            	mLatitude.setText( String.valueOf(mUIGeofence.getLatitude()));
            	mLongitude.setText( String.valueOf(mUIGeofence.getLongitude()));
            	mAddress.setText(mUIGeofence.getAddress());
            	Log.d(TAG, "set UI text" + mUIGeofence.getLatitude() +mUIGeofence.getLongitude() + mUIGeofence.getAddress() );
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        // Choose what to do based on the request code
        switch (requestCode) 
        {
        // If the request code matches the code sent in onConnectionFailed
        case LocationFinderUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST :
            switch (resultCode) 
            {          
            // If Google Play services resolved the problem
            case Activity.RESULT_OK:
                if (LocationFinderUtils.REQUEST_TYPE.ADD == mRequestType) 
                {
                	// ADD request
                    // Toggle the request flag and send a new request
                    mGeofenceRequester.setInProgressFlag(false);
                    // Restart the process of adding the current geofences
                    mGeofenceRequester.addGeofences(mCurrentGeofences);
                }
                else if (LocationFinderUtils.REQUEST_TYPE.REMOVE == mRequestType )
                {
                    // REMOVE request
                    // Toggle the removal flag and send a new removal request
                    mGeofenceRemover.setInProgressFlag(false);
                    if (LocationFinderUtils.REMOVE_TYPE.INTENT == mRemoveType) 
                    {
                        // If the removal was by Intent: Restart the removal of all geofences for the PendingIntent
                        mGeofenceRemover.removeGeofencesByIntent(mGeofenceRequester.getRequestPendingIntent());
                    } 
                    else 
                    {
                        // If the removal was by a List of geofence IDs:  Restart the removal of the geofence list
                        mGeofenceRemover.removeGeofencesById(mGeofenceIdsToRemove);
                    }
                }
            break;
            default:
                // Report that Google Play services was unable to resolve the problem.
                Log.d(TAG, "no resulution");
            break;
            }
        default:
           // Report that this Activity received an unknown requestCode
           Log.d(TAG, "unknown activity request code" + requestCode);
        break;
        }	
    }
    
    /*
     * Whenever the Activity resumes, reconnect the client to Location
     * Services and reload the last geofences that were set
     */
    @Override
    protected void onResume() 
    {
        super.onResume();
        // Register the broadcast receiver to receive status updates
        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, mIntentFilter);
    }
    
    @Override
    protected void onPause() 
    {
        super.onPause();
        mPrefs.setGeofence("1", mUIGeofence);
    }
    
    /**
     * Verify that Google Play services is available before making a request.
     * @return true if Google Play services is available, otherwise false
     */
    private boolean servicesConnected() 
    {
        // Check that Google Play services is available
        if (ConnectionResult.SUCCESS == GooglePlayServicesUtil.isGooglePlayServicesAvailable(this)) 
        {
            Log.d(TAG, "Play services available");
            return true;
        }
        else 
        {
        	 Log.e(TAG, "Play services NOT available!");
            return false;
        }
    }

    @Override
	public boolean onOptionsItemSelected(MenuItem item) 
    {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        this.finish();
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
    
    @Override
    public void onDestroy()
    {
    	super.onDestroy();
    }
    

    /**
     * Get the geofence parameters for each geofence and add them to
     * a List. Create the PendingIntent containing an Intent that
     * Location Services sends to this app's broadcast receiver when
     * Location Services detects a geofence transition. Send the List
     * and the PendingIntent to Location Services.
     */
    public void onRegisterGeofence() 
    {
    	/*
         * Record the request as an ADD. If a connection error occurs,
         * the app can automatically restart the add request if Google Play services
         * can fix the error
         */
        mRequestType = LocationFinderUtils.REQUEST_TYPE.ADD;
        /*
         * Create a version of geofence 1 that is "flattened" into individual fields. This
         * allows it to be stored in SharedPreferences.
         */
        mUIGeofence = new SimpleGeofence(
            "1",
            Double.valueOf(mLatitude.getText().toString()),
            Double.valueOf(mLongitude.getText().toString()),
            Float.valueOf(accuracy),
            GEOFENCE_EXPIRATION_IN_MILLISECONDS,
            Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT,
            mAddress.getText().toString());

        Log.d(TAG, "Set lat:" + Double.valueOf(mLatitude.getText().toString()) + " long:" 
          + Double.valueOf(mLongitude.getText().toString())+mAddress.getText().toString());
        
        // Store this flat version in SharedPreferences
        mPrefs.setGeofence("1", mUIGeofence);
        mCurrentGeofences.add(mUIGeofence.toGeofence());
  
        // Start the request. Fail if there's already a request in progress
        try
        {
            // Try to add geofences
            mGeofenceRequester.addGeofences(mCurrentGeofences);
        } 
        catch (UnsupportedOperationException e) 
        {
            Toast.makeText(this, R.string.add_geofences_already_requested_error, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Called when the user clicks the "Remove geofence 1" button
     * @param view The view that triggered this callback
     */
    public void onUnregisterGeofence()
    {
        mRemoveType = LocationFinderUtils.REMOVE_TYPE.INTENT;
        if (!servicesConnected()) 
            return;

        // Try to remove the geofence
        try 
        {  	// INTENT
            mGeofenceRemover.removeGeofencesByIntent( mGeofenceRequester.getRequestPendingIntent());
        }
        catch (IllegalArgumentException e) 
        {
            e.printStackTrace();
        }
        catch (UnsupportedOperationException e) 
        {
            // Notify user that previous request hasn't finished.
            Toast.makeText(this, R.string.remove_geofences_already_requested_error, Toast.LENGTH_LONG).show();
        }

        /* Remove the geofence by creating a List of geofences to
         * remove and sending it to Location Services. The List
         * contains the id of geofence 1 ("1").
         * The removal happens asynchronously; Location Services calls
         * onRemoveGeofencesByPendingIntentResult() (implemented in
         * the current Activity) when the removal is done.
         */
        mGeofenceIdsToRemove = Collections.singletonList("1");
        mRemoveType = LocationFinderUtils.REMOVE_TYPE.LIST;
        try 
        {
        	mGeofenceRemover.removeGeofencesById(mGeofenceIdsToRemove);
        } 
        catch (IllegalArgumentException e) 
        {
        	e.printStackTrace();
        }
        catch (UnsupportedOperationException e) 
        {
        	// Notify user that previous request hasn't finished.
        	Toast.makeText(this, R.string.remove_geofences_already_requested_error, Toast.LENGTH_LONG).show();
        }
    }
    
    /**
     * Define a Broadcast receiver that receives updates from connection listeners and
     * the geofence transition service.
     */
    public class GeofenceSampleReceiver extends BroadcastReceiver 
    {
        /*
         * Define the required method for broadcast receivers
         * This method is invoked when a broadcast Intent triggers the receiver
         */
        @Override
        public void onReceive(Context context, Intent intent) 
        {
            // Check the action code and determine what to do
            String action = intent.getAction();
            if (TextUtils.equals(action, LocationFinderUtils.ACTION_GEOFENCE_ERROR)) 
            {
                // Intent contains information about errors in adding or removing geofences
                handleGeofenceError(context, intent);
            } 
            else if ( TextUtils.equals(action, LocationFinderUtils.ACTION_GEOFENCES_ADDED) ||
                    TextUtils.equals(action, LocationFinderUtils.ACTION_GEOFENCES_REMOVED))
            {
                // Intent contains information about successful addition or removal of geofences
                handleGeofenceStatus(context, intent);
            }
            else if (TextUtils.equals(action, LocationFinderUtils.ACTION_GEOFENCE_TRANSITION)) 
            {
            	// Intent contains information about a geofence transition
                handleGeofenceTransition(context, intent);
            }
            else
            {
            	// The Intent contained an invalid action
                Log.e(TAG,"invaid action" + action);
                Toast.makeText(context, R.string.invalid_action, Toast.LENGTH_LONG).show();
            }
        }

        /**
         * If you want to display a UI message about adding or removing geofences, put it here.
         *
         * @param context A Context for this component
         * @param intent The received broadcast Intent
         */
        private void handleGeofenceStatus(Context context, Intent intent) 
        {
            String msg = intent.getStringExtra(LocationFinderUtils.EXTRA_GEOFENCE_STATUS);
            Log.i(TAG, msg);
        }

        /**
         * Report geofence transitions to the UI
         *
         * @param context A Context for this component
         * @param intent The Intent containing the transition
         */
        private void handleGeofenceTransition(Context context, Intent intent) 
        {
            String msg = intent.getStringExtra(LocationFinderUtils.EXTRA_GEOFENCE_STATUS);
            Log.i(TAG, msg);     	
        }

        /**
         * Report addition or removal errors to the UI, using a Toast
         *
         * @param intent A broadcast Intent sent by ReceiveTransitionsIntentService
         */
        private void handleGeofenceError(Context context, Intent intent) 
        {
            String msg = intent.getStringExtra(LocationFinderUtils.EXTRA_GEOFENCE_STATUS);
            Log.e(TAG, msg);
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
        }
    }
}
