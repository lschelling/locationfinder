package com.example.locationfinder;


import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.text.InputFilter;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationListener;

public class MainActivity extends  Activity implements
GetAddressTaskFragment.TaskCallbacks,
GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener,
LocationListener
{
	private GetAddressTaskFragment mTaskFragment;
	private static final String TAG = "MainActivity";
	private static final int minLatitude = -90;
	private static final int maxLatitude = 90;
	private static final int minLongitude = -180;
	private static final int maxLongitude = 180;
	private static final double targetAccuracy = 50.0;
	private LocationClient mLocationClient;
	private LocationRequest locationrequest;
	private ProgressDialog progressDialog;

	// === some helpers first
	/** set the content of the address text field */
	private void updateAddress(String addressStr) 
	{
   	  TextView myaddress = (TextView) findViewById(R.id.textAddress);
	  myaddress.setText(addressStr);
	}

	/** set the min max filter for a edit text field */
	private void setMinMaxFilter(int editTextId, int min, int max) {
		EditText et = (EditText) findViewById(editTextId);
		et.setFilters(new InputFilter[]{ new InputFilterRange(min, max)});
	}

	/** gets a random number within a range */
	private double randomWithRange(double min, double max)
	{
		double range = Math.abs(max-min);
		return (Math.random()*range-range/2);
	}
	
	/** shows up a cancel dialog with a message */
	public void showCancelMessage(Context context, int msgId) {
		AlertDialog.Builder ad = new AlertDialog.Builder(context);
		ad.setMessage(msgId);
		ad.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			  public void onClick(DialogInterface dialog, int whichButton) { /* Canceled. */}
			});
		ad.show();
	}
	
	// === implement Activity
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		android.app.FragmentManager fm = getFragmentManager();
		mTaskFragment = (GetAddressTaskFragment) fm.findFragmentByTag("task");
		if(null == mTaskFragment)
		{
			mTaskFragment = new GetAddressTaskFragment();
			fm.beginTransaction().add(mTaskFragment, "task").commit();
			updateAddress("");
		}
		else
			updateAddress(mTaskFragment.getAddress());
		
		mLocationClient = new LocationClient(this, this, this);

		// set filter ranges for the lat and longitude input fields 
		setMinMaxFilter(R.id.editLatitude, minLatitude, maxLatitude);
		setMinMaxFilter(R.id.editLongitude, minLongitude, maxLongitude);
		
		// if the activity has been started from the PickLocationFromMap activity
		// the input fields must be initialized with the picked location
		String s = this.getIntent().getStringExtra("latitude");
		if (null != s)
		{
			EditText et = (EditText) findViewById(R.id.editLatitude);
			et.setText(s);
		}
		s = this.getIntent().getStringExtra("longitude");
		if (null != s)
		{
			EditText et = (EditText) findViewById(R.id.editLongitude);
			et.setText(s);
		}
		
		// add listener to Random button
		Button button = (Button)  findViewById(R.id.buttonRandomLatLong);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				updateAddress("");
				EditText et = (EditText) findViewById(R.id.editLatitude);
				et.setText(Double.toString(randomWithRange(minLatitude, maxLatitude)));
				et = (EditText) findViewById(R.id.editLongitude);
				et.setText(Double.toString(randomWithRange(minLongitude, maxLongitude)));
			}	
		});
		
		// add listener to Get Address button
		button = (Button)  findViewById(R.id.buttonGetAddress);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) 
			{
				EditText etLong =(EditText) findViewById(R.id.editLongitude);
				EditText etLat =(EditText) findViewById(R.id.editLatitude);			
				if( (0 < etLong.getText().toString().length()) &&
					(0 < etLat.getText().toString().length()))
				{
					if( 	(null != ((ConnectivityManager) MainActivity.this
							.getSystemService(Context.CONNECTIVITY_SERVICE))
							.getActiveNetworkInfo()) 
							&&
							(((ConnectivityManager) MainActivity.this
									.getSystemService(Context.CONNECTIVITY_SERVICE))
									.getActiveNetworkInfo().isConnected())
					   )
					{
						mTaskFragment.findAddress(Double.parseDouble(etLat.getText().toString()), Double.parseDouble(etLong.getText().toString()));
						Log.d(TAG, "getAddressTask started");
					}
					else
						MainActivity.this.showCancelMessage(v.getContext(), R.string.msg_no_network);
				}
				else
					MainActivity.this.showCancelMessage(v.getContext(), R.string.msg_invalid_data);
			}	
		}
		);
	}

    @Override
    public void onResume()
    {
    	super.onResume();
    }
    
    @Override
    protected void onPause()
    {
      super.onPause();
    }
   
    
	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		if (progressDialog!=null)
			progressDialog.dismiss();
	}

	 /*
     * Called when the Activity becomes visible.
     */
    @Override
    protected void onStart() {
        super.onStart();
        // Connect the client.
        mLocationClient.connect();
    }

    /*
     * Called when the Activity is no longer visible.
     */
    @Override
    protected void onStop() {
        // Disconnecting the client invalidates it.
        mLocationClient.disconnect();
        super.onStop();
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
		case R.id.action_about:	
			Intent intent = new Intent(MainActivity.this, AboutActivity.class);
			startActivity(intent);
			return true;
			
		case R.id.action_get_location:
			 locationrequest = LocationRequest.create();
			 locationrequest.setInterval(100);
			 mLocationClient.requestLocationUpdates(locationrequest, this);
			 progressDialog = new ProgressDialog(this);
			 progressDialog.setTitle(R.string.msg_get_location);
			 progressDialog.setMessage(getString(R.string.msg_please_wait));
			 progressDialog.setCancelable(true);
			 progressDialog.setIndeterminate(true);
			 progressDialog.show();
			 updateAddress("");
			return true;
			
		case R.id.action_set_location:
			intent = new Intent(MainActivity.this, GeoFenceActivity.class);
			EditText etLat =(EditText) findViewById(R.id.editLatitude);
			intent.putExtra("latitude", etLat.getText().toString());
			EditText etLong =(EditText) findViewById(R.id.editLongitude);
			intent.putExtra("longitude", etLong.getText().toString());
			TextView address =(TextView) findViewById(R.id.textAddress);
			intent.putExtra("address", address.getText().toString());
			startActivity(intent);
			return true;
			
		case R.id.action_pick_location:
			intent = new Intent(MainActivity.this, PickLocationFromMap.class);
			etLat =(EditText) findViewById(R.id.editLatitude);
			intent.putExtra("latitude", etLat.getText().toString());
			etLong =(EditText) findViewById(R.id.editLongitude);
			intent.putExtra("longitude", etLong.getText().toString());
			startActivity(intent);
			return true;			
			default:
				Log.v(TAG, "unhandled menu event");
				break;
		}
		return false;		
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	// === implement OnConnectionFailedListener interface
	
	@Override
	public void onConnectionFailed(ConnectionResult result) {	}

	@Override
	public void onConnected(Bundle connectionHint) {}

	@Override
	public void onDisconnected() {	}
	
	@Override
	public void onLocationChanged(Location location) 
	{
	  if(location!=null)
	  {
		  if(location.getAccuracy() < targetAccuracy)
		  {
			  EditText et = (EditText) findViewById(R.id.editLatitude);
			  et.setText(Double.toString(location.getLatitude()));
			  et = (EditText) findViewById(R.id.editLongitude);
			  et.setText(Double.toString(location.getLongitude()));
			  mLocationClient.removeLocationUpdates(this);
			  progressDialog.dismiss();
		  }
		  else
		  {
			  Log.v(TAG, "Location Request :" + location.getLatitude() + "," + location.getLongitude() + "insufficient accuracy " + location.getAccuracy());
		  }
	  }
	  else
	  {
		  Log.v(TAG, "location=null");
	  }
	}
	
	// === implement GetAddressTaskFragment.TaskCallbacks interface
	
	@Override
	public void onPreExecute() {}

	@Override
	public void onProgressUpdate(int percent) {}

	@Override
	public void onCancelled() {}

	@Override
	public void onPostExecute() 
	{
		updateAddress(mTaskFragment.getAddress());
	}
}
