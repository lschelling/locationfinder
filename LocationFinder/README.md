LocationFinder
========================================================================

Investierte Zeit
------------------------------------------------------------------------
 - Artan  10h Android Vorkenntnisse
 - Lorenz 16h keine Erfahrung in der Android Entwicklung

Zur Implementierung
------------------------------------------------------------------------
Die Aufgaben A1 bis A6 haben keinere grösseren Probleme verursacht.

Die Eingabeüberprüfung haben wir mittels InputFilter realisiert.

Die Configuration Changes während dem eine Activy aktiv ist war etwas
schwierig zu realisieren.Vorallem gibt es dazu verschiedene Ansätze,
wir haben uns dabei für den Ansatz mittels Task Fragments entschieden,
da er bei der aktuellen Android Versionen empfohlen wird.

Die Geofences Implementation basiert auf dem Beispielcode von Google.
Er war recht schwierig zu verstehen und es hat einige Zeit gedauert
bis wir die erste Notification bekommen haben.Ohne diesen Code
wäre es schwierig gewesen.

 [1] http://developer.android.com/training/location/geofencing.html

Zusätzliches Funktionen
------------------------------------------------------------------------
Damit die Applikation auch zu etwas sinnvollem zugebrauchen ist und
um den Test zu erleichtern, haben wir noch die Funktion implementiert
eine Position in den GoogleMaps zu wählen. Nachdem das Ganze mit den
Google API Keys erst einmal funktioniert hat, war die Realisierung
einfach und hat Spass gemacht.

Tests
------------------------------------------------------------------------
Wir haben die Applikation nur auf einer sehr beschränkten Anzahl 
Geräte testen können.

 - Asus Fonepad
 - Samsung Galaxy Note 2
 - Samsung Galaxy S2
 - Samsung Galasy Tab 2 7.0

Generelle Eindrücke
------------------------------------------------------------------------
Es handelt sich beim LocationFinder um eine kleine Applikation mit
einer überschaubaren Anzahl an Activities. Aber schon hier zeigt es
sich dass es in den Details oft schwierig ist, die GUI konsistent
zu implementieren. zB Unterschied zwischen Back-Button und UpAction.
Oder die Daten nach einem Configuration Change wieder richtig
darzustellen.usw

Ausserdem habe ich (Lorenz) den Eindruck, dass sich die Android API
einem starken Veränderungen unterworfen ist. Ich hoffe wir haben
in dieser App immer die zur Zeit aktuellen Funktionen gebraucht.

Insgesamt gesehen war die Übung sehr zeitintensiv, war aber auch
interessant und hat viel Spass gemacht.

